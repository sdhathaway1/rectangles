package com.sdh;

/**
 * User: Scott Hathaway
 * Date: 9/12/14
 * <p>
 * The Point class is used to create objects that represent an ordered pair of numbers (x,y) in a coordinate plane.
 * </p>
 */
public class Point {

    /**
     * Value along the X axis in the coordinate plane.
     */
    public int x;

    /**
     * Value along the Y axis in the coordinate plane.
     */
    public int y;


    /**
     * Creates a Point with the given ordered pair.
     *
     * @param x The horizontal position in the coordinate plane.
     * @param y The vertical position in the coordinate plane.
     */
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }


    /**
     * Point equivalence test. Two Points with the same coordinates are considered equal.
     *
     * @param obj Object for comparison.
     * @return true or false based on equality.
     */
    @Override
    public boolean equals(Object obj) {
        //Same reference
        if (this == obj)
            return true;

        if (obj instanceof Point) {
            Point p = (Point) obj;

            if (p.x == this.x && p.y == this.y) {
                return true;
            }
        }

        return false;
    }

    @Override
    public int hashCode() {
        final String s = String.valueOf(x) + ":" + String.valueOf(y);
        return s.hashCode();
    }


    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }


}
