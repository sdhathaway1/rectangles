package com.sdh;

/**
 * User: Scott Hathaway
 * Date: 9/13/14
 * <p>
 * This class can be used to create simple line objects, which contain 4 {@link Point} objects.
 * </p>
 */
public class Line {

    /**
     * x coordinate of the starting point.
     */
    public int x1;

    /**
     * x coordinate of the ending point.
     */
    public int x2;

    /**
     * y coordinate of the starting point.
     */
    public int y1;

    /**
     * y coordinate of the ending point.
     */
    public int y2;

    /**
     * Creates a new Line from the given {@link Point}s
     * @param p1 Start of the line.
     * @param p2 End of the line.
     */
    public Line(Point p1, Point p2) {
        this.x1 = p1.x;
        this.x2 = p2.x;
        this.y1 = p1.y;
        this.y2 = p2.y;
    }

    /**
     * Returns the length of this line.
     * @return Returns an int containing the lenght of the line.
     */
    public int length() {
        return Math.abs(x2 - x1);
    }

    /**
     * Returns if a Line contains another Line.
     * @param l The line to check for containment.
     * @return true/false
     */
    public boolean contains(Line l) {
        return this.contains(new Point(l.x1,l.y1)) && this.contains(new Point(l.x2,l.y2));
    }

    /**
     * Returns if a line contains a point or not.
     * @param p The point to check.
     * @return true/false
     */
    public boolean contains(Point p) {
        return p.x >= x1 && p.x <= x2 && p.y >= y1 && p.y <= y2;
    }


    @Override
    public String toString() {
        return "Line{" +
                "(" + x1 +
                ", " + y1 +
                "), (" + x2 +
                ", " + y2 +
                ")}";
    }
}
