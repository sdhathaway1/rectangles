package com.sdh;

import java.util.*;

/**
 * User: Scott Hathaway
 * Date: 9/12/14
 * <p>
 * The Rectangle class is used to create Rectangle objects and provides methods to test whether
 * this Rectangle intersects, contains, or is adjacent to another Rectangle.
 * The Rectangle is presumed to exist in a coordinate plane with an x,y origin of 0,0.
 * </p>
 */
public class Rectangle {

    /**
     * The origin point for this rectangle. This is the top left point.
     */
    public Point origin;

    /**
     * The height of the rectangle.
     */
    public int height;

    /**
     * The width of the rectangle.
     */
    public int width;

    /**
     * The bottom left point.
     */
    protected Point bottomLeft;

    /**
     * The top right point;
     */
    protected Point topRight;

    /**
     * The bottom right point.
     */
    protected Point bottomRight;

    /**
     * Constructs a new Rectangle at the given origin with the specified height and width.
     *
     * @param origin A {@link Point} with coordinates of the upper left corner of the
     * @param height Int value for the height of this rectangle.
     * @param width  Int value for the width of this rectangle.
     * @throws IllegalArgumentException Thrown if the origin is null or height or width &lt; 0
     */
    public Rectangle(Point origin, int height, int width) throws IllegalArgumentException {

        if (origin == null)
            throw new IllegalArgumentException("Rectangle origin can not be null.");

        if (height < 0)
            throw new IllegalArgumentException("The Rectangle can not have a negative height.");

        if (width < 0)
            throw new IllegalArgumentException("The Rectangle can not have a negative width.");

        this.origin = origin;
        this.height = height;
        this.width = width;

        bottomLeft = new Point(this.origin.x, this.origin.y - this.height);
        topRight = new Point(this.origin.x + this.width, this.origin.y);
        bottomRight = new Point(this.origin.x + this.width, this.origin.y - this.height);
    }

    /**
     * Returns the points of intersection, if they exist, between this Rectangle and the Rectangle specified.
     * Implements a Line Sweep Algorithm
     *
     * @param r The Rectangle to check for intersection.
     * @return A {@link Set} of {@link Point} objects containing the coordinates of intersection.
     */
    public Set<Point> intersection(Rectangle r) {
        final Set<Point> points = new HashSet<>();

        final List<Line> lines = this.getLines();
        lines.addAll(r.getLines());

        final SortedMap<Integer, Integer> active = new TreeMap<>();
        final ArrayList<Event> events = new ArrayList<>();

        for (Line line : lines) {

            if (line.y1 != line.y2)
                events.add(new Event(EventType.EVENT_VERTICLE, line.x1, line));
            else {
                if(line.length() > 0) {
                    events.add(new Event(EventType.EVENT_START, line.x1, line));
                    events.add(new Event(EventType.EVENT_END, line.x2, line));
                }
            }

        }

        Collections.sort(events);

        for (Event e : events) {

            switch (e.type) {
                case EVENT_START: {

                    final Integer count = active.get(e.line.y1);
                    active.put(e.line.y1, count == null ? 1 : count + 1);
                    break;
                }
                case EVENT_END: {

                    int count = active.get(e.line.y1) - 1;

                    if(count > 0)
                        active.put(e.line.y1, count);
                    else
                        active.remove(e.line.y1);
                    break;
                }
                case EVENT_VERTICLE: {

                    final SortedMap<Integer, Integer> view = active.subMap(e.line.y1 + 1, e.line.y2);

                    for(Map.Entry<Integer,Integer> i : view.entrySet()) {
                        for(int j = 0; j < i.getValue(); j++) {
                            points.add(new Point(e.line.x1, i.getKey()));
                        }
                    }

                    break;
                }

            }

        }


        return points;
    }


    /**
     * Returns whether this rectangle contains the specified rectangle.
     *
     * @param r The Rectangle to check for containment.
     * @return true if the rectangle is contained in this rectangle and false if not.
     */
    public boolean contains(Rectangle r) {

        //Top left
        if(r.origin.x < this.origin.x || r.origin.y > this.origin.y)
            return false;

        //Top right
        if(r.topRight.x > this.topRight.x || r.topRight.y > this.topRight.y)
            return false;

        //Bottom left
        if(r.bottomLeft.x < this.bottomLeft.x || r.bottomLeft.y < this.bottomLeft.y)
            return false;

        //Bottom right
        if(r.bottomRight.x > this.bottomRight.x || r.bottomRight.y < this.bottomRight.y)
            return false;


        return true;

    }



    /**
     * Returns whether this rectangle is adjacent to the specified rectangle.
     *
     * @param r The Rectangle to check for adjacency.
     * @return true if the rectangle is adjacent and false if not.
     */
    public boolean adjacent(Rectangle r) {
        final Line[] rLines = new Line[4];
        r.getLines().toArray(rLines);

        final Line rTop = rLines[0];
        final Line rLeft = rLines[1];
        final Line rBottom = rLines[2];
        final Line rRight = rLines[3];

        final Line[] thisLines = new Line[4];
        this.getLines().toArray(thisLines);

        final Line thisTop = thisLines[0];
        final Line thisLeft = thisLines[1];
        final Line thisBottom = thisLines[2];
        final Line thisRight = thisLines[3];

        if(thisTop.contains(rBottom) || rBottom.contains(thisTop))
            return true;
        else if(thisLeft.contains(rRight) || rRight.contains(thisLeft))
            return true;
        else if(thisRight.contains(rLeft) || rLeft.contains(thisRight))
            return true;
        else if(thisBottom.contains(rTop) || rTop.contains(thisBottom))
            return true;

        return false;
    }


    private List<Line> getLines() {
        final ArrayList<Line> lines = new ArrayList<>();

        lines.add(new Line(this.origin, topRight));
        lines.add(new Line(bottomLeft, this.origin));
        lines.add(new Line(bottomLeft, bottomRight));
        lines.add(new Line(bottomRight, topRight));

        return lines;
    }


    private static enum EventType {
        EVENT_END,
        EVENT_VERTICLE,
        EVENT_START
    }

    private static class Event implements Comparable<Event> {

        public EventType type;
        public int x;
        public Line line;

        public Event(EventType type, int x, Line line) {

            this.type = type;
            this.x = x;
            this.line = line;

        }

        @Override
        public int compareTo(Event e) {
            if (x != e.x)
                return x - e.x;
            else
                return type.compareTo(e.type);

        }


    }


}
