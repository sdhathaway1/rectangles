import com.sdh.Line;
import com.sdh.Point;
import com.sdh.Rectangle;
import org.junit.Test;

import java.util.Set;

/**
 * User: Scott Hathaway
 * Date: 9/12/14
 *
 * Class used to test features of the Rectangle API.
 *
 */
public class RectangleTests {


    //Point Tests

    @Test
    public void testPointConstruction() throws Exception {

        Point p = new Point(10, 5);
        assert p.x == 10 : "p.x does not equal 10";
        assert p.y == 5 : "p.y does not equal 5";

    }


    @Test
    public void testEqualityOfPoints() throws Exception {
        Point p1 = new Point(10, 5);
        assert p1.equals(p1) : "Reflexive equality test failed.";

        Point p2 = new Point(10, 5);
        assert p1.equals(p2) && p2.equals(p1) : "Symmetric equality failed.";

        Point p3 = new Point(10, 5);
        assert p1.equals(p2) && p2.equals(p3) && p1.equals(p3) : "Transitive equality failed.";

        assert p1.equals(null) == false : ("Object should not equal null.");

    }

    @Test
    public void pointHashTest() throws Exception {
        Point p1 = new Point(10, 5);
        Point p2 = new Point(10, 5);
        assert p1.hashCode() == p2.hashCode() : "Hash codes do not match.";

        Point p3 = new Point(11, 8);
        assert p1.hashCode() != p3.hashCode() : "Hash codes should not match.";

    }

    //Line Tests
    @Test
    public void lineConstructionTest() throws Exception {


        Line l = new Line(new Point(5, 10), new Point(10, 10));

        assert l.x1 == 5 : "Line start x does not equal 5";
        assert l.x2 == 10 : "Line end x does not equal 10";
        assert l.y1 == 10 : "Line start y does not equal 10";
        assert l.y2 == 10 : "Line end y does not equal 10";


    }


    @Test
    public void lineContainmentTest() {
        Line l1 = new Line(new Point(5, 10), new Point(15, 10));
        Point p1 = new Point(8, 10);
        assert l1.contains(p1) : "l1 should contain p1";

        Line l2 = new Line(new Point(6,10), new Point(8,10));
        assert l1.contains(l2) : "l1 should contain l2";
    }

    //Rectangle Tests

    @Test
    public void testRectangleConstruction() throws Exception {

        Rectangle r = new Rectangle(new Point(1, 10), 10, 20);
        assert r.origin.equals(new Point(1, 10)) : "Rectangle origin is not equal to a Point of 10,5";
        assert r.height == 10 : "Rectangle height is not equal to 10";
        assert r.width == 20 : "Rectangle width is not equal to 20";

    }

    @Test
    public void testRectangleIllegalArgument() throws Exception {

        try {
            Rectangle r = new Rectangle(null, 10, 20);
        } catch (Exception e) {
            assert e instanceof IllegalArgumentException : "Wrong exception thrown by constructor.";
        }

        try {
            Rectangle r = new Rectangle(new Point(5, 10), -1, 20);
        } catch (Exception e) {
            assert e instanceof IllegalArgumentException : "Wrong exception thrown by constructor.";
        }

        try {
            Rectangle r = new Rectangle(new Point(5, 10), 10, -2);
        } catch (Exception e) {
            assert e instanceof IllegalArgumentException : "Wrong exception thrown by constructor.";
        }


    }


    //Intersection Test

    @Test
    public void intersectionTest1() throws Exception {

        Rectangle r1 = new Rectangle(new Point(1, 10), 10, 10);
        Rectangle r2 = new Rectangle(new Point(2, 5), 10, 8);

        Set<Point> points = r1.intersection(r2);
        printPoints(points);

        assert points.size() == 2 : "Number of points should be 2 but is " + points.size();
        assert points.contains(new Point(2, 0)) : "Intersection is missing first point.";
        assert points.contains(new Point(10, 0)) : "Intersection is missing second point.";

    }

    @Test
    public void intersectionTest2() throws Exception {

        Rectangle r1 = new Rectangle(new Point(1, 10), 10, 10);
        Rectangle r2 = new Rectangle(new Point(2, 5), 8, 0);

        Set<Point> points = r1.intersection(r2);
        printPoints(points);

        assert points.size() == 1 : "Number of points should be 1 but is " + points.size();
        assert points.contains(new Point(2, 0)) : "Intersection is missing first point.";

    }

    @Test
    public void intersectionTest3() throws Exception {

        Rectangle r1 = new Rectangle(new Point(1, 10), 10, 10);
        Rectangle r2 = new Rectangle(new Point(12, 10), 10, 10);

        Set<Point> points = r1.intersection(r2);
        printPoints(points);

        assert points.size() == 0 : "Number of points should be 0 but is " + points.size();

    }

    //Containment tests

    @Test
    public void containmentTest1() throws Exception {

        Rectangle r1 = new Rectangle(new Point(1, 10), 10, 10);
        Rectangle r2 = new Rectangle(new Point(2, 9), 7, 8);

        assert r1.contains(r2) : "Rectangle 1 should contain rectangle 2.";
        assert !r2.contains(r1) : "Rectangle 2 should not contain rectangle 1";
    }

    @Test
    public void containmentNoIntersection() throws Exception {

        Rectangle r1 = new Rectangle(new Point(1, 10), 10, 10);
        Rectangle r2 = new Rectangle(new Point(2, 9), 7, 8);

        assert r1.contains(r2) : "Rectangle 1 should contain rectangle 2.";
        assert !r2.contains(r1) : "Rectangle 2 should not contain rectangle 1";
        assert r1.intersection(r2).size() == 0 : "R1 and R2 should not intersect.";
    }

    @Test
    public void containmentNoIntersection2() throws Exception {

        Rectangle r1 = new Rectangle(new Point(10, 20), 100, 100);
        Rectangle r2 = new Rectangle(new Point(15, 15), 50, 50);

        assert r1.contains(r2) : "Rectangle 1 should contain rectangle 2.";
        assert !r2.contains(r1) : "Rectangle 2 should not contain rectangle 1";
        assert r1.intersection(r2).size() == 0 : "R1 and R2 should not intersect.";
    }

    @Test
    public void intersectionNoContainmentTest() throws Exception {

        Rectangle r1 = new Rectangle(new Point(1, 10), 10, 10);
        Rectangle r2 = new Rectangle(new Point(2, 5), 10, 8);

        Set<Point> points = r1.intersection(r2);
        printPoints(points);

        assert points.size() == 2 : "Number of points should be 2 but is " + points.size();
        assert points.contains(new Point(2, 0)) : "Intersection is missing first point.";
        assert points.contains(new Point(10, 0)) : "Intersection is missing second point.";
        assert r1.contains(r2) == false : "R1 should not contain R2";

    }

    //Adjacency Test

    @Test
    public void adjacentTest1() throws Exception {

        Rectangle r1 = new Rectangle(new Point(1, 10), 5, 5);
        Rectangle r2 = new Rectangle(new Point(6, 10), 5, 5);

        assert r1.adjacent(r2) : "r1 should be adjacent to r2.";
        assert r2.adjacent(r1) : "r2 should be adjacent to r1.";

    }

    @Test
    public void adjacentSubLine() throws Exception {

        Rectangle r1 = new Rectangle(new Point(1, 10), 5, 5);
        Rectangle r2 = new Rectangle(new Point(2, 5), 5, 3);

        assert r1.adjacent(r2) : "r1 should be adjacent to r2.";
        assert r2.adjacent(r1) : "r2 should be adjacent to r1.";

    }

    @Test
    public void adjacentNoIntersection() throws Exception {

        Rectangle r1 = new Rectangle(new Point(1, 10), 5, 5);
        Rectangle r2 = new Rectangle(new Point(6, 10), 5, 5);

        assert r1.adjacent(r2) : "r1 should be adjacent to r2.";
        assert r2.adjacent(r1) : "r2 should be adjacent to r1.";
        assert r1.intersection(r2).size() == 0 : "r1 should not intersect r2";
    }

    @Test
    public void adjacentNoContainment() throws Exception {

        Rectangle r1 = new Rectangle(new Point(1, 10), 5, 5);
        Rectangle r2 = new Rectangle(new Point(6, 10), 5, 5);

        assert r1.adjacent(r2) : "r1 should be adjacent to r2.";
        assert r2.adjacent(r1) : "r2 should be adjacent to r1.";
        assert !r1.contains(r2) : "r1 should not contain r2.";
    }


    private void printPoints(Set<Point> points) {
        for (Point p : points) {
            System.out.println(p);
        }
    }


}
