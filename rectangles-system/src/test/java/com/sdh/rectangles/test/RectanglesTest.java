package com.sdh.rectangles.test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sdh.rectangles.RectanglesApplication;
import com.sdh.rectangles.RectanglesConfiguration;
import com.sdh.rectangles.api.Point;
import com.sdh.rectangles.api.RectangleAPI;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.ArrayList;

/**
 * User: Scott Hathaway
 * Date: 9/13/14
 */
public class RectanglesTest {


    final private ObjectMapper m = new ObjectMapper();

    @ClassRule
    public static final DropwizardAppRule<RectanglesConfiguration> rule =
            new DropwizardAppRule<>(RectanglesApplication.class,
                    ClassLoader.getSystemClassLoader().getResource("config.yaml").getPath());


    @Test
    public void testAPI() throws Exception {
        final Point p = new Point(1, 10);
        final RectangleAPI r = new RectangleAPI();
        r.setOrigin(p);
        r.setLength(10);
        r.setWidth(10);

        final Point p1 = new Point(2, 8);
        final RectangleAPI r2 = new RectangleAPI();
        r2.setOrigin(p1);
        r2.setLength(5);
        r2.setWidth(5);

        ArrayList<RectangleAPI> rectangles = new ArrayList<>();
        rectangles.add(r);
        rectangles.add(r2);

        final Client client = new Client();
        final WebResource resource = client.resource("http://localhost:8080/api/rectangle");
        ClientResponse response = resource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, m.writeValueAsString(rectangles));

        JsonNode node = getMessage(response);
        assert node.get("adjacent").asBoolean() == false : "Should not be adjacent";
        assert node.get("containment").asBoolean() == true : "Should be contained";
        assert node.get("intersection").size() == 0 : "Should have no intersections";


    }

    private JsonNode getMessage(ClientResponse response) throws IOException {
        String entity = response.getEntity(String.class);
        JsonNode rootNode = m.readValue(m.getFactory().createParser(entity), JsonNode.class);
        return rootNode;
    }
}
