package com.sdh.rectangles.api;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * User: Scott Hathaway
 * Date: 9/13/14
 */
public class RectangleAPI {

    @JsonProperty
    private Point origin;

    @JsonProperty
    private int length;

    @JsonProperty
    private int width;

    public Point getOrigin() {
        return origin;
    }

    public void setOrigin(Point origin) {
        this.origin = origin;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
