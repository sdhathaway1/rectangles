package com.sdh.rectangles.api;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * User: Scott Hathaway
 * Date: 9/13/14
 */
public class Point {

    @JsonProperty
    private int x;

    @JsonProperty
    private int y;


    public Point() {

    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }


    public int getX() {
        return x;
    }

    public void setX(int x) {

        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
