package com.sdh.rectangles.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * User: Scott Hathaway
 * Date: 9/13/14
 */
public class Result {

    @JsonProperty
    private ArrayList<Point> intersection;

    @JsonProperty
    private Boolean adjacent;

    @JsonProperty
    private Boolean containment;

    public ArrayList<Point> getIntersection() {
        return intersection;
    }

    public void setIntersection(ArrayList<Point> intersection) {
        this.intersection = intersection;
    }

    public Boolean getAdjacent() {
        return adjacent;
    }

    public void setAdjacent(Boolean adjacent) {
        this.adjacent = adjacent;
    }

    public Boolean getContainment() {
        return containment;
    }

    public void setContainment(Boolean containment) {
        this.containment = containment;
    }
}
