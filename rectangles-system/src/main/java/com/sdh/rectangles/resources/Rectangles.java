package com.sdh.rectangles.resources;

import com.sdh.Point;
import com.sdh.Rectangle;
import com.sdh.rectangles.api.RectangleAPI;
import com.sdh.rectangles.api.Result;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * User: Scott Hathaway
 * Date: 9/13/14
 */

@Path("/rectangle")
public class Rectangles {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Result evalRectangles(@Valid List<RectangleAPI> rectangleList) {
        RectangleAPI[] rectangles = new RectangleAPI[2];
        rectangleList.toArray(rectangles);

        Rectangle r1 = new Rectangle(new Point(rectangles[0].getOrigin().getX(), rectangles[0].getOrigin().getY()),
                rectangles[0].getLength(), rectangles[0].getWidth());
        Rectangle r2 = new Rectangle(new Point(rectangles[1].getOrigin().getX(), rectangles[1].getOrigin().getY()),
                rectangles[1].getLength(), rectangles[1].getWidth());
        Result result = new Result();
        result.setAdjacent(r1.adjacent(r2));
        result.setContainment(r1.contains(r2));
        result.setIntersection(pointToPoint(r1.intersection(r2)));

        return result;
    }

    private ArrayList<com.sdh.rectangles.api.Point> pointToPoint(Set<Point> points) {
        ArrayList<com.sdh.rectangles.api.Point> apiPoints = new ArrayList<>();

        for(Point p : points) {
           apiPoints.add(new com.sdh.rectangles.api.Point(p.x, p.y));
        }

        return apiPoints;
    }


}
