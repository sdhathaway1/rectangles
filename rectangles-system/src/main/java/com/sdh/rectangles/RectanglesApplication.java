package com.sdh.rectangles;

import com.sdh.rectangles.resources.Rectangles;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

/**
 * User: Scott Hathaway
 * Date: 9/13/14
 */
public class RectanglesApplication extends Application<RectanglesConfiguration> {

    public static void main(String[] args) throws Exception {
        new RectanglesApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<RectanglesConfiguration> bootstrap) {
        bootstrap.addBundle(new AssetsBundle("/assets/","/","index.html","static"));
    }

    @Override
    public void run(RectanglesConfiguration rectanglesConfiguration, Environment environment) throws Exception {

        environment.jersey().setUrlPattern("/api/*");

        environment.jersey().register(new Rectangles());

    }
}
